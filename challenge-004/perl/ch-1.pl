#!/usr/bin/perl

=pod

Write a script to output the same number of PI digits as the size of your script. Say, if your script size is 10, it should print 3.141592653.

=cut

use Modern::Perl '2010';

use Math::BigFloat qw(bpi);

my $num = $ARGV[0] // ( stat($0) )[7];

say bpi($num);
