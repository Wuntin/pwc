#!/usr/bin/perl
#

=pod

You are given a file containing a list of words (case insensitive 1 word per line) and a list of letters. Print each word from the file that can be made using only letters from the list. You can use each letter only once (though there can be duplicates and you can use each of them once), you don’t have to use all the letters. (Disclaimer: The challenge was proposed by Scimon Proctor)

=cut

use Modern::Perl '2010';

my %letters = letter_list(shift);

unless (@_) {
    say "Usage: $0 <letters> <word-file>";

    exit 1;
}

foreach (<>) {
    chomp;

    say if can_construct($_);
}

sub can_construct {
    my %l = letter_list(shift);

    foreach ( keys %l ) {
        return 0 unless ( $letters{$_} // 0 ) >= $l{$_};
    }

    return 1;
}

sub letter_list {
    my %l = ();

    foreach ( split //, lc( shift // '' ) ) {
        $l{$_}++;
    }

    return %l;
}
