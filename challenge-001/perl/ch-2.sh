#!/bin/sh

# Write a one-liner to solve the FizzBuzz problem and print the numbers 1 through 20. However, any number divisible by 3 should be replaced by the word ‘fizz’ and any divisible by 5 by the word ‘buzz’. Those numbers that are both divisible by 3 and 5 become ‘fizzbuzz’.

perl -E 'foreach (1 .. $ARGV[0] // 20) { my($o); $o .= "fizz" if $_ % 3 == 0; $o .= "buzz" if $_ % 5 == 0; say($o || $_); }' $@
