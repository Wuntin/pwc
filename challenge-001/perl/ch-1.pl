#!/usr/bin/perl

=pod

Write a script to replace the character ‘e’ with ‘E’ in the string ‘Perl Weekly Challenge’. Also print the number of times the character ‘e’ is found in the string.

=cut

use Modern::Perl '2010';

my $str = $ARGV[0] // 'Perl Weekly Challenge';
my $num = $str =~ tr/e/E/;

say $str;
say $num;
