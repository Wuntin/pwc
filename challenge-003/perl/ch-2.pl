#!/usr/bin/perl

=pod

Create a script that generates Pascal Triangle. Accept number of rows from the command line. The Pascal Triangle should have at least 3 rows. For more information about Pascal Triangle, check this wikipedia page [https://en.wikipedia.org/wiki/Pascal%27s_triangle].

=cut

use Modern::Perl '2010';

use List::Util qw(max);

my $num_rows = max( 3, $ARGV[0] // 3 );
my @rows = ( [1] );

while ( scalar(@rows) < $num_rows ) {
    my $last = 0;
    push @rows,
      [ ( map { my $new = $_ + $last; $last = $_; $new; } @{ $rows[-1] } ), 1 ];
}

my $width = length( join( '   ', @{ $rows[-1] } ) );

foreach my $row (@rows) {
    say pad( join( '   ', @{$row} ) );
}

sub pad {
    my $str = shift;
    my $padding = ' ' x ( ( $width - length($str) ) / 2 );

    return $padding . $str;
}
