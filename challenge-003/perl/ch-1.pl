#!/usr/bin/perl

=pod

Create a script to generate 5-smooth numbers, whose prime divisors are less or equal to 5. They are also called Hamming/Regular/Ugly numbers. For more information, please check this wikipedia [https://en.wikipedia.org/wiki/Regular_number].

=cut

use Modern::Perl '2010';

my $N = $ARGV[0] // 60;
my $numbers = { 1 => 1 };

foreach my $number ( 2 .. $N ) {
    if ( is_hamming_number($number) ) {
        $numbers->{$number} = 1;
    }
}

say join( ', ', sort { $a <=> $b; } keys %{$numbers} );

sub is_hamming_number {
    my $number = shift;

    return 1 if $numbers->{ $number / 2 };
    return 1 if $numbers->{ $number / 3 };
    return 1 if $numbers->{ $number / 5 };

    return 0;
}
