#!/usr/bin/perl

=pod

Write a script or one-liner to remove leading zeros from positive numbers.

=cut

use Modern::Perl '2010';

my $num = $ARGV[0] || '0042';

$num =~ s/^0+//;

say $num;
