#!/usr/bin/perl

=pod

Write a script that can convert integers to and from a base35 representation, using the characters 0-9 and A-Y. Dave Jacoby came up with nice description about base35, in case you needed some background.

=cut

use Modern::Perl '2010';

my @CHARS = ( 0 .. 9, 'A' .. 'Y' );

my $num = $ARGV[0] // 42;

say base35($num);

sub base35 {
    my $num = shift;
    my $rep = '';

    while ($num) {
        $rep = $CHARS[ $num % 35 ] . $rep;

        $num = int( $num / 35 );
    }

    return $rep;
}
